/**************************************************************************//**
 * @file
 * @brief Segment LCD Config
 * @author Energy Micro AS
 * @version 1.0.2
 ******************************************************************************
 * @section License
 * <b>(C) Copyright 2012 Energy Micro AS, http://www.energymicro.com</b>
 *******************************************************************************
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 * 4. The source and compiled code may only be used on Energy Micro "EFM32"
 *    microcontrollers and "EFR4" radios.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: Energy Micro AS has no
 * obligation to support this Software. Energy Micro AS is providing the
 * Software "AS IS", with no express or implied warranties of any kind,
 * including, but not limited to, any implied warranties of merchantability
 * or fitness for any particular purpose or warranties against infringement
 * of any proprietary rights of a third party.
 *
 * Energy Micro AS will not be liable for any consequential, incidental, or
 * special damages, or any other relief, or for any claim by any third party,
 * arising from your use of this Software.
 *
 *****************************************************************************/
#ifndef __SEGMENTLCDCONFIG_H
#define __SEGMENTLCDCONFIG_H

#include "em_lcd.h"

#ifdef __cplusplus
extern "C" {
#endif

/* LCD Controller Prescaler (divide LFACLK / 64) */
/* LFACLK_LCDpre = 512 Hz */
/* Set FDIV=0, means 512/1 = 512 Hz */
/* With octaplex mode, 512/16 => 32 Hz Frame Rate */
#define LCD_CMU_CLK_PRE         cmuClkDiv_64
#define LCD_CMU_CLK_DIV         cmuClkDiv_1

#define LCD_BOOST_LEVEL         lcdVBoostLevel3


#define LCD_INIT_DEF \
{ true,\
  lcdMuxStatic,\
  lcdBiasStatic,\
  lcdWaveLowPower,\
  lcdVLCDSelVDD, \
  lcdConConfVLCD }

#define LCD_NUMBER_OFF() \
do { \
  LCD_SegmentSetLow(0, 0x0007FFFC, 0x00000000); \
} while (0)



#define LCD_ALL_SEGMENTS_OFF() \
do { \
  LCD_SegmentSetLow(0, 0x00007FFFC, 0x00000000);\
} while(0)

#define LCD_ALL_SEGMENTS_ON() \
do { \
  LCD_SegmentSetLow(0, 0x0007FFFC, 0xFFFFFFFF);\
} while(0)

#define LCD_SEGMENTS_ENABLE() \
do { \
LCD_SegmentRangeEnable(lcdSegment0_3, true);\
LCD_SegmentRangeEnable(lcdSegment4_7,true);\
LCD_SegmentRangeEnable(lcdSegment8_11,true);\
LCD_SegmentRangeEnable(lcdSegment12_15, true);\
LCD_SegmentRangeEnable(lcdSegment16_19, true);\
} while(0)

#define LCD_DISPLAY_ENABLE() \
do { \
  ;\
} while(0)

#define EFM_DISPLAY_DEF {\
  .Number      = {\
    {\
      .bit[0] = 10, .bit[1] = 9, .bit[2] = 8, .bit[3] = 7,\
      .bit[4] = 6, .bit[5] = 4, .bit[6] = 5,\
    },\
    {\
      .bit[0] = 14, .bit[1] = 13, .bit[2] = 12, .bit[3] = 18,\
      .bit[4] = 17, .bit[5] = 15, .bit[6] = 16,\
    },\
    {\
      .bit[0] = 3, .bit[1] = 3, .bit[2] = 3, .bit[3] = 3,\
      .bit[4] = 3, .bit[5] = 3, .bit[6] = 3,\
    }\
  }\
}


#ifdef __cplusplus
}
#endif

#endif
