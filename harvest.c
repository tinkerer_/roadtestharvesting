/**************************************************************************//**
 * @file
 * @brief Internal temperature sensor example for EFM32_G8xx_STK
 * @details
 *   Show temperature using internal sensor on the EFM32.
 * @note
 *   Due to bugs in earlier chip revisions, this demo only works correctly for
 *   revision C chips or later.
 *
 * @par Usage
 * @li Buttons toggle Celcius and Fahrenheit temperature modes
 *
 * @author Energy Micro AS
 * @version 3.0.3
 ******************************************************************************
 * @section License
 * <b>(C) Copyright 2012 Energy Micro AS, http://www.energymicro.com</b>
 *******************************************************************************
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 * 4. The source and compiled code may only be used on Energy Micro "EFM32"
 *    microcontrollers and "EFR4" radios.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: Energy Micro AS has no
 * obligation to support this Software. Energy Micro AS is providing the
 * Software "AS IS", with no express or implied warranties of any kind,
 * including, but not limited to, any implied warranties of merchantability
 * or fitness for any particular purpose or warranties against infringement
 * of any proprietary rights of a third party.
 *
 * Energy Micro AS will not be liable for any consequential, incidental, or
 * special damages, or any other relief, or for any claim by any third party,
 * arising from your use of this Software.
 *
 *****************************************************************************/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "em_device.h"
#include "em_chip.h"
#include "em_emu.h"
#include "em_gpio.h"
#include "em_adc.h"
#include "em_cmu.h"
#include "segmentlcd.h"
#include "rtcdrv.h"
#include "bsp_trace.h"
#include <math.h>

#define BETA_VALUE 3800.0 //corrected for this sensor
#define R_25C 100000.0
#define T_25C (298.15-5.0) //corrected for this sensor

#define RTH_WHITE 25.0
#define CTH_WHITE 4.0
#define RTH_YOLK  17.0
#define CTH_YOLK  3.0
#define TSTART_EGG 18

typedef enum DISPLAY_TEMP {DISPLAY_EGG_TEMP, DISPLAY_WATER_TEMP} Display_temp_t;

volatile int sample_timeout = 0;
volatile int reset_egg_timer = 0;
volatile Display_temp_t display = DISPLAY_EGG_TEMP;


struct egg_s
{
    float tyolk;
    float twhite;
};

/**************************************************************************//**
 * @brief GPIO Interrupt handler (PD7) Switch temperature display
 *****************************************************************************/
void GPIO_ODD_IRQHandler(void)
{
    /* Acknowledge interrupt */
    GPIO_IntClear(1 << 7);
    if(display == DISPLAY_EGG_TEMP)
        display = DISPLAY_WATER_TEMP;
    else
        display = DISPLAY_EGG_TEMP;
}

/**************************************************************************//**
 * @brief GPIO Interrupt handler (PD8) Fahrenheit
 *****************************************************************************/
void GPIO_EVEN_IRQHandler(void)
{
    /* Acknowledge interrupt */
    GPIO_IntClear(1 << 8);
    reset_egg_timer = 1;

}

/**************************************************************************//**
 * @brief Setup GPIO interrupt to change temp. display
 *****************************************************************************/
void gpioSetup(void)
{
    /* Configure PB10 as input and enable interrupt  */
    GPIO_PinModeSet(gpioPortD, 7, gpioModeInputPull, 1);
    GPIO_IntConfig(gpioPortD, 7, false, true, true);

    NVIC_ClearPendingIRQ(GPIO_EVEN_IRQn);
    NVIC_EnableIRQ(GPIO_EVEN_IRQn);

    /* Configure PD9 as input and enable interrupt */
    GPIO_PinModeSet(gpioPortD, 8, gpioModeInputPull, 1);
    GPIO_IntConfig(gpioPortD, 8, false, true, true);

    NVIC_ClearPendingIRQ(GPIO_ODD_IRQn);
    NVIC_EnableIRQ(GPIO_ODD_IRQn);

    /*Enable output of pin*/
    GPIO_PinModeSet(gpioPortD, 1, gpioModePushPull, 0);
    GPIO_PinModeSet(gpioPortB, 8, gpioModePushPull, 0);

}

/**************************************************************************//**
 * @brief ADC0 interrupt handler. Simply clears interrupt flag.
 *****************************************************************************/
void ADC0_IRQHandler(void)
{
    ADC_IntClear(ADC0, ADC_IF_SINGLE);
}


/**************************************************************************//**
 * @brief Initialize ADC for external temperature sensor readings in single poin
 *****************************************************************************/
void setupExternalTempSensor(void)
{
    /* Base the ADC configuration on the default setup. */
    ADC_Init_TypeDef init = ADC_INIT_DEFAULT;
    ADC_InitSingle_TypeDef sInit = ADC_INITSINGLE_DEFAULT;

    /* Initialize timebases */
    init.timebase = ADC_TimebaseCalc(0);
    init.prescale = ADC_PrescaleCalc(400000,0);
    ADC_Init(ADC0, &init);

    /* Set input to temperature sensor. Reference must be 1.25V */
    sInit.reference = adcRefVDD;
    sInit.input = adcSingleInpCh0;
    ADC_InitSingle(ADC0, &sInit);

    /* Setup interrupt generation on completed conversion. */
    ADC_IntEnable(ADC0, ADC_IF_SINGLE);
    NVIC_EnableIRQ(ADC0_IRQn);
}

void setupSWO(void)
{
    uint32_t *dwt_ctrl = (uint32_t *) 0xE0001000;
    uint32_t *tpiu_prescaler = (uint32_t *) 0xE0040010;
    uint32_t *tpiu_protocol = (uint32_t *) 0xE00400F0;

    CMU->HFPERCLKEN0 |= CMU_HFPERCLKEN0_GPIO;
    /* Enable Serial wire output pin */
    GPIO->ROUTE |= GPIO_ROUTE_SWOPEN;
#if defined(_EFM32_GIANT_FAMILY)
    /* Set location 0 */
    GPIO->ROUTE = (GPIO->ROUTE & ~(_GPIO_ROUTE_SWLOCATION_MASK)) | GPIO_ROUTE_SWLOCATION_LOC0;

    /* Enable output on pin - GPIO Port F, Pin 2 */
    GPIO->P[5].MODEL &= ~(_GPIO_P_MODEL_MODE2_MASK);
    GPIO->P[5].MODEL |= GPIO_P_MODEL_MODE2_PUSHPULL;
#else
    /* Set location 1 */
    GPIO->ROUTE = (GPIO->ROUTE & ~(_GPIO_ROUTE_SWLOCATION_MASK)) | GPIO_ROUTE_SWLOCATION_LOC1;
    /* Enable output on pin */
    GPIO->P[2].MODEH &= ~(_GPIO_P_MODEH_MODE15_MASK);
    GPIO->P[2].MODEH |= GPIO_P_MODEH_MODE15_PUSHPULL;
#endif
    /* Enable debug clock AUXHFRCO */
    CMU->OSCENCMD = CMU_OSCENCMD_AUXHFRCOEN;

    while(!(CMU->STATUS & CMU_STATUS_AUXHFRCORDY));

    /* Enable trace in core debug */
    CoreDebug->DHCSR |= 1;
    CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;

    /* Enable PC and IRQ sampling output */
    *dwt_ctrl = 0x400113FF;
    /* Set TPIU prescaler to 16. */
    *tpiu_prescaler = 0xf;
    /* Set protocol to NRZ */
    *tpiu_protocol = 2;
    /* Unlock ITM and output data */
    ITM->LAR = 0xC5ACCE55;
    ITM->TCR = 0x10009;
}

void CalculateTyolk(struct egg_s *egg, float twater)
{
    float tyolk_new, twhite_new;
    twhite_new = egg->twhite + ((((twater - egg->twhite)/RTH_WHITE)-((egg->twhite - egg->tyolk)/RTH_YOLK))*(1/CTH_WHITE)*2);
    tyolk_new  = egg->tyolk  + (((egg->twhite- egg->tyolk)/RTH_YOLK) * (1/CTH_YOLK) * 2);
    egg->tyolk = tyolk_new;
    egg->twhite = twhite_new;
}

void InitEgg(struct egg_s *egg, float tstart)
{
    egg->twhite = tstart;
    egg->tyolk  = tstart;
}

void DisplayUpdate(float tyolk, float twater)
{
    if(display == DISPLAY_EGG_TEMP)
        SegmentLCD_Number((int)(tyolk));
    else
        SegmentLCD_Number((int)(twater));
}
void HandleSampleTimeout(void)
{
    sample_timeout=1;
}
/**************************************************************************//**
 * @brief  Main function
 *****************************************************************************/
int main(void)
{
    //SYSTEM_ChipRevision_TypeDef revision;
    struct egg_s egg;
    float resistor_value;
    float r_inf;
    char string[8];
    uint32_t temp;

    /* Chip errata */
    CHIP_Init();

    /* If first word of user data page is non-zero, enable eA Profiler trace */
    BSP_TraceProfilerSetup();
    r_inf = R_25C*exp(-BETA_VALUE/T_25C);
    /* Enable peripheral clocks */
    CMU_ClockEnable(cmuClock_HFPER, true);
    CMU_ClockEnable(cmuClock_GPIO, true);
    CMU_ClockEnable(cmuClock_ADC0, true);

    /* Initialize LCD controller without boost */
    SegmentLCD_Init(false);
    SegmentLCD_AllOff();


    /* Check for revision after revision B. Chips with revision earlier than */
    /* Revision C has known problems with the internal temperature sensor. */
    /* Display a warning in this case */


    /* Enable board control interrupts */
    gpioSetup();

    /* Setup ADC for sampling internal temperature sensor. */
    //setupSensor();
    setupExternalTempSensor();
    /* Main loop - just read temperature and update LCD */
    setupSWO();
    InitEgg(&egg, TSTART_EGG);
//  SegmentLCD_ARing(7,1);
    while (1)
    {
        float temperature;
        static int beeper = 3;
        /* Start one ADC sample */
        GPIO_PinOutSet(gpioPortD, 1);
        RTCDRV_Trigger(2, NULL);
        EMU_EnterEM2(true);
        ADC_Start(ADC0, adcStartSingle);
        /* Wait in EM1 for ADC to complete */
        EMU_EnterEM1();
        GPIO_PinOutClear(gpioPortD, 1);
        temp = ADC_DataSingleGet(ADC0);

        /* Convert ADC sample to Fahrenheit / Celsius and print string to display */

        resistor_value = (temp*47000.0)/(4096.0-temp);
        temperature = BETA_VALUE/log(resistor_value/r_inf);
        temperature -= 273;
        //Convert to Celsius scale
        CalculateTyolk(&egg, temperature);
        snprintf(string, 8, "%2d,%1d%%C", (int)(egg.tyolk), abs(((int)(egg.tyolk*10))%10));
        DisplayUpdate(egg.tyolk, temperature);
        if(reset_egg_timer)
        {
            reset_egg_timer = 0;
            beeper  = 3;
            InitEgg(&egg,199);//TSTART_EGG);
        }

//    SegmentLCD_Write(string);
        /* Sleep for 2 seconds in EM 2 */
        sample_timeout = 0;
        if ((egg.tyolk >= 85) && (beeper>0)) //Hard egg
        {
            beeper--;
            RTCDRV_Trigger(100, HandleSampleTimeout);
            //Buzzer On
            GPIO_PinOutSet(gpioPortB, 8);
            EMU_EnterEM2(true);
            while(!sample_timeout)
            {
                EMU_EnterEM2(true);
            }
            //Buzzer Off
            GPIO_PinOutClear(gpioPortB, 8);
        }
        sample_timeout = 0;
        RTCDRV_Trigger(2000, HandleSampleTimeout);
        EMU_EnterEM2(true);
        while(!sample_timeout)
        {
            DisplayUpdate(egg.tyolk, temperature);
            EMU_EnterEM2(true);
        }
    }
}
